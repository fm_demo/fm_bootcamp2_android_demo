import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('D:\\katalon\\DemoAppV2.apk', true)

Mobile.tap(findTestObject('Object Repository/Mobile Objects (Dany)/home_quick_login'), 0)

Mobile.setText(findTestObject('Object Repository/Mobile Objects (Dany)/login_field_email'), 'fmdemo@mailinator.com', 0)

Mobile.setText(findTestObject('Object Repository/Mobile Objects (Dany)/login_field_password'), 'dem0pa55!', 0)

Mobile.tap(findTestObject('Object Repository/Mobile Objects (Dany)/login_button_login'), 0)

WebUI.delay(15)

Mobile.tap(findTestObject('Object Repository/Mobile Objects (Dany)/bottombar_profile'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile Objects (Dany)/button_invoice'), 0)

Mobile.scrollToText('Completed', FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Object Repository/Mobile Objects (Dany)/invoice_item_fmdemo_cancel_EVN31082300007'), 
    0)

Mobile.tap(findTestObject('Object Repository/Mobile Objects (Dany)/invoice_item_fmdemo_cancel_EVN31082300007'),
	0)

Mobile.verifyElementExist(findTestObject('Object Repository/Mobile Objects (Dany)/invoice_detail_status_cancel'), 0)

Mobile.closeApplication()

