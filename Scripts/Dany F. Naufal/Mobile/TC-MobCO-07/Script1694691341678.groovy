import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('D:\\katalon\\DemoAppV2.apk', true)

Mobile.tap(findTestObject('Object Repository/Mobile Objects (Dany)/home_quick_login'), 0)

Mobile.setText(findTestObject('Object Repository/Mobile Objects (Dany)/login_field_email'), 'fmdemo2@mailinator.com', 0)

Mobile.setText(findTestObject('Object Repository/Mobile Objects (Dany)/login_field_password'), 'DemoUser2', 0)

Mobile.tap(findTestObject('Object Repository/Mobile Objects (Dany)/login_button_login'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile Objects (Dany)/btn_cart_b'), 0)

Mobile.checkElement(findTestObject('Object Repository/Mobile Objects (Dany)/cart_checkbox_workshop_b'), 0)

Mobile.setText(findTestObject('Object Repository/Mobile Objects (Dany)/cart_voucher_input'), 'voucher', 0)

Mobile.tap(findTestObject('Object Repository/Mobile Objects (Dany)/cart_voucher_apply'), 0)

Mobile.delay(15, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('Object Repository/Mobile Objects (Dany)/cart_msg_bad_voucher'), 0)

Mobile.closeApplication()

